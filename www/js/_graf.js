//fix startswith
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position){
      position = position || 0;
      return this.substr(position, searchString.length) === searchString;
  };
};

function dayYear(datum) {   // d is a Date object
  var d = new Date(Date.parse(datum));
  var yn = d.getFullYear();
  var mn = d.getMonth();
  var dn = d.getDate();
  var d1 = new Date(yn,0,1,12,0,0); // noon on Jan. 1
  var d2 = new Date(yn,mn,dn,12,0,0); // noon on input date
  var ddiff = Math.round((d2 - d1) / 864e5);
  return ddiff + 1;
};

function isYear(datum) {
  return datum.DATE.startsWith(this);
};

function dateForm(datum) {
  formatDate = d3.time.format("%-d. %-m. %Y")
  return formatDate(new Date(datum))
};

var makeLine = d3.svg.line()
  .x(function(d, i) {
    return x(dayYear(d.DATE));
  })
  .y(function(d, i) {
    return y(parseFloat(d.TMIN));
  });

var cze = d3.locale({
  "decimal": ".",
  "thousands": ",",
  "grouping": [3],
  "currency": ["$", ""],
  "dateTime": "%a %b %e %X %Y",
  "date": "%m/%d/%Y",
  "time": "%H:%M:%S",
  "periods": ["AM", "PM"],
  "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
  "shortDays": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
  "months": ["leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec"],
  "shortMonths": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
});

d3.time.format = cze.timeFormat;

var wi = document.documentElement.clientWidth;
if(wi > 1000) {
  wi = 1000;
};

var margin = {top: 20, right: 15, bottom: 60, left: 60}
    , width = wi - margin.left - margin.right
    , height = (wi * 0.5) - margin.top - margin.bottom;
  
  var x = d3.scale.linear()
            .domain([1, 365])
            .range([ 0, width ]);
  
  var y = d3.scale.linear()
          .domain([-25, 22])
          .range([ height, 0 ]);

  var xTime = d3.time.scale()
          .domain([new Date('1990-01-01'), new Date('1990-12-31')])
          .range([ 0, width ]);

  var chart = d3.select('#temp_graf')
    .append('svg:svg')
    .attr('width', width + margin.right + margin.left)
    .attr('height', height + margin.top + margin.bottom)
    .attr('class', 'chart')

  var main = chart.append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
    .attr('width', width)
    .attr('height', height)
    .attr('class', 'main')   
      
  var xAxis = d3.svg.axis()
    .scale(xTime)
    .ticks(d3.time.months.utc, 1)
    .tickFormat(d3.time.format.utc('%B'))
    .orient('bottom');

  main.append('g')
    .attr('transform', 'translate(20,' + (height + 10) + ')')
    .attr('class', 'main axis date')
    .call(xAxis);

  var yAxis = d3.svg.axis()
    .tickSize(0)
    .scale(y)
    .orient('left');

  main.append('g')
    .attr('transform', 'translate(-15, 0)')
    .attr('class', 'main axis date')
    .call(yAxis);

  var toolTip = main.append('g')
    .attr('class', 'tooltip')
    .attr('transform', 'translate(' + (width * 0.4) + ', ' + (height * 0.76) + ')')
    .append('text')
    .text('Myší vyberte den.')

  var g = main.append("svg:g");

  d3.csv("./data/libus.csv", function(data) {
    var dots = g.selectAll("scatter-dots")
      .data(data)
      .enter().append("svg:circle")
          .attr("cx", function (d,i) { 
            return x(dayYear(d.DATE)); 
          } )
          .attr("cy", function (d) { 
            return y(parseFloat(d.TMIN)); 
          } )
          .attr('r', function (d) {
            if (d.DATE.startsWith('2017')) {
              return 4;
            } else {
              return 2;
            }
          })
          .attr('fill', function (d) {
            if (d.DATE.startsWith('2017')) {
              return '#e31a1c';
            } else {
              return '#a6cee3';
            }
          })
          .style("opacity", 0.7);
    
    var yearLine;
    dots.on('mouseover', function (evt) {
      var yearData = data.filter(isYear, evt.DATE.split('-')[0])
      

      yearLine = g.append("svg:path").attr("d", makeLine(yearData)).attr('class', 'yearline');

      d3.select('.tooltip').select('text').text('Dne ' + dateForm(evt.DATE) + ' byla nejnižší teplota ' + evt.TMIN + ' °C');

    })
    dots.on('mouseout', function (evt) {
      yearLine.remove();
      d3.select('.tooltip').select('text').text('Myší vyberte den.');
    });

    //trendlajna
    d3.csv("./data/tmin_mean.csv", function(data) {
      trendLine = g.append("svg:path").attr("d", makeLine(data)).attr('class', 'trendline');
    });           
  });