---
title: "Chladněji než obvykle? Letošní mrazy jsou mimořádné, ukazují data za čtyři desetiletí"
perex: "Mrazy, které tento a minulý týden zasáhly Prahu, jsou z pohledu posledních několika desetiletí mimořádné. Ukazují to data, která o počasí v hlavním městě od roku 1973 získal Český rozhlas."
description: "Mrazy, které tento a minulý týden zasáhly celé Česko včetně hlavního města, jsou z pohledu posledního půlstoletí mimořádné. Ukazují to data, která o počasí v Praze od roku 1973 získal Český rozhlas."
authors: ["Jan Cibulka", "Michal Zlatkovský"]
published: "17. ledna 2017"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
socialimg: https://interaktivni.rozhlas.cz/prazska-zima/media/most.jpg
url: "prazska-zima"
libraries: [d3]
styles: ["./styl/graf.css"]
recommended:
  - link: https://interaktivni.rozhlas.cz/horko-ve-mestech/
    title: Mapa pražského horka: Stromy ochladí okolí o několik stupňů
    perex: Hlavní město hledá cesty, jak se vypořádat s tropickými vedry.
    image: https://interaktivni.rozhlas.cz/horko-ve-mestech/media/socimg.png
  - link: http://www.rozhlas.cz/zpravy/politika/_zprava/pametnikum-letosni-sucho-pripomina-rok-1947-tehdy-se-promitlo-i-do-politiky--1521880
    title: Pamětníkům sucho roku 2015 připomíná rok 1947. Tehdy se promítlo i do politiky
    perex: Důsledky sucha v roce 1947 byly mnohem hrozivější, než se původně čekalo. Kvůli katastrofální neúrodě vázlo zásobování základními potravinami. V některých oblastech Slovenska hrozil hladomor. 
    image: http://media.rozhlas.cz/_obrazek/3448898--zemedelci-sucho-pole-ilustracni-foto--1-950x0p0.jpeg
---

Pražané, kterým letošní zima připadá studenější než obvykle, se ve svém odhadu nemýlí. Nejnižší naměřené teploty v první polovině ledna byly většinou hluboko pod průměrem. Druhá lednová středa byla dokonce druhým nejchladnějším 11. lednem za posledních 45 let. Ukazují do data z meteorologická stanice v pražské Libuši, které Český rozhlas získal [přes americkou NOAA](http://www.noaa.gov/).

Teplotní rekordy padaly po celém Česku, od [Litoměřicka](http://litomericky.denik.cz/zpravy_region/rekord-teto-zimy-doksany-hlasi-minus-18-stupnu-celsia-20170112.html) po tradičně mrazivou Šumavu, kde meteorologové naměřili [35 stupňů pod nulou](http://zpravy.e15.cz/domaci/udalosti/cesko-sviraji-letos-rekordni-mrazy-na-sumave-namerili-35-stupnu-1327442). Na dlouholetá minima se ale teplota dostala i ve městech včetně toho hlavního - i navzdory tomu, že se o nich mluví jako o teplotních ostrovech.

Teplotní data po dnech a rocích si můžete prohlédnout v následujícím grafu. Červené tečky ukazují teplotní rekordy v letošním lednu. Modrá čára ukazuje průměrnou nejnižší naměřenou teplotu od roku 1973. Jednotlivé roky se zobrazí po najetí myši na modré body.

<aside class="big">
  <div id='temp_graf'></div>
</aside>

Z prvních patnácti dnů letošního ledna překonala nejnižší teplota průměr celkem jedenáctkrát. Celkem čtyři dny se navíc pohybovala pod deseti stupni. 11. ledna dokonce klesla až na -17 stupňů Celsia. V Praze kvůli tomu na desítkách míst popraskalo vodovodní potrubí. Na následky podchlazení zemřeli v hlavním městě od minulého pátku celkem čtyři lidé.

Mrazy podle předpovědí zasáhnou Prahu i celé Česko i v tomto týdnu. Teplota v metropoli má v pátek v noci klesnout až na 13 stupňů pod nulou. „Třetí lednový týden předpokládáme teplotně podprůměrný, lze očekávat i delší období celodenních mrazů. Čtvrtý lednový týden by podle současných předpokladů měl být opět průměrný," uvádí [prognóza Českého hydrometeorologického ústavu](http://www.rozhlas.cz/zpravy/domaci/_zprava/1684226). 